import React, { Component } from 'react'; // 모드 모듈즈에 있다.

class LifecycleExample extends Component {
    // 제일 처음 시작된다.
    constructor(props) {
        super(props); // 수퍼 부모는 extends로 상속받았다. Component
        this.state = { // 없으면 만든다.
            count: 0, // 카운트라는 초기값 0 ;
        };
    }

    componentDidMount() {
        console.log('Component mounted');
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.count !== prevState.count) { // prevState getSnapshotBeforeUpdate 에서 왔다.
            console.log('Count updated:', this.state.count);
        }
    }

    componentWillUnmount() {
        console.log('Component will unmount');
    }

    handleIncrement = () => {
        this.setState((prevState) => ({ count: prevState.count + 1 })); // 원에이 바인딩이여서 이렇게 해주는거다. this.state.count 하면 나중에 문제가 터진다.
    };

    render() {
        return (
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleIncrement}>Increment</button>
            </div>
        );
    }
}

export default LifecycleExample;